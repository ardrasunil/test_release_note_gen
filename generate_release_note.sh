#!/bin/bash

apt-get update -qy
apt-get install -y jq


#store the constants
ACCESS_TOKEN="glpat-5QQHBNppoxXknAGKkBfg"
GITLAB_API_URL="https://gitlab.com/api/v4"

echo $CI_PROJECT_ID
echo $CI_COMMIT_REF_NAME
echo $GITLAB_API_URL

#get mr id
MR_ID=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_API_URL/projects/$CI_PROJECT_ID/merge_requests?source_branch=$CI_COMMIT_REF_NAME" | jq -r '.[0].iid')
echo "Merge Request ID : $MR_ID"

#API to get the merge request commit message 
release_note=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_API_URL/projects/$CI_PROJECT_ID/merge_requests/$MR_ID/commits" | jq -r '.[] | "\(.title) by \(.committer_name) on \(.committed_date)"')
#echo "-- COMMITS -- "
#echo "$release_note"

#API to generate release for the respective merge request
##data for api call
release_name="Release Note : $CI_COMMIT_REF_NAME"
release_tag="$CI_COMMIT_REF_NAME"
release_description="Commits of Merge Request for branch : $CI_COMMIT_REF_NAME
"
release_description+="$release_note"
release_ref="refs/heads/$CI_COMMIT_REF_NAME"
##to check 
echo "$release_name"
echo "$release_tag"
echo "$release_description"
##api call to generate release note 
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" --data "name=$release_name" --data "tag_name=$release_tag" --data "description=$release_note" --data "ref=$release_ref" "$GITLAB_API_URL/projects/$CI_PROJECT_ID/releases"
echo "script executed successfully"

